#!/bin/bash

x=`ls /overlay/ | wc -l`

if [ $x -gt 0 ] ; then 
    cp -a /overlay/* / ;
fi

source /etc/apache2/envvars && exec /usr/sbin/apache2 -DFOREGROUND
